<?php
namespace Plazathemes\ObserverTest\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;


class CustomerLogin implements ObserverInterface {

  /** @var \Magento\Framework\Logger\Monolog */
  protected $logger;
  
  public function __construct(
    \Psr\Log\LoggerInterface $loggerInterface
  ) {
    $this->logger = $loggerInterface;
  }

  /**
   * This is the method that fires when the event runs. 
   * 
   * @param Observer $observer
   */
  public function execute( Observer $observer ) {
    
    $customer = $observer->getCustomer();
    $this->logger->debug($customer->getName());
  }
}